import { DBOrders } from '../../enum/dborders.enum';
import { ApiProperty } from '@nestjs/swagger';
/**
 * @ignore
 */
export class PaginationSortsDTO {
  @ApiProperty()
  priority: number;
  @ApiProperty()
  field: string;
  @ApiProperty()
  order: DBOrders; //ASC, DESC
}
