import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { AuthService } from './auth.service';
import { LocalStrategy } from './guards/local/local.strategy';
import { JwtStrategy } from './guards/jwt/jwt.strategy';
import { UsersService } from '../modules/users/users.service';
import { UsersModule } from '../modules/users/users.module';
import { AuthController } from './auth.controller';
import { ConfigModule } from '@nestjs/config';
import { ConfigService } from '@nestjs/config';
import { ConfigKeys } from '../common/enum/configkeys.enum';

/**
 * Aquí tambien podemos documentar.
 */
@Module({
  imports: [
    ConfigModule,
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (_configService: ConfigService) => ({
        secret: _configService.get(ConfigKeys.JWT_SECRET),
        signOptions: {
          expiresIn: _configService.get(ConfigKeys.JWT_EXPIRATION),
        },
      }),
      inject: [ConfigService],
    }),
    UsersModule,
  ],
  controllers: [AuthController],
  providers: [LocalStrategy, JwtStrategy, AuthService, UsersService],
  exports: [AuthService],
})
export class AuthModule {}
