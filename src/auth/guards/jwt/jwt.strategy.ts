/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { JWTPayLoadDTO } from '../../../auth/dto/jwtPayload.dto';
import { LoginIdentityDTO } from '../../../auth/dto/loginIdentity.dto';
import { ConfigService } from '@nestjs/config';
import { ConfigKeys } from '../../../common/enum/configkeys.enum';
/**
 * Estrategia para validacion del jwt
 */
@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly _configService: ConfigService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: _configService.get<string>(ConfigKeys.JWT_SECRET),
    });
  }

  async validate(payload: JWTPayLoadDTO): Promise<LoginIdentityDTO> {
    //payload proviene del contenido del token
    return {
      //esto que retornamos queda en req.user, para el resto de la solicitud
      id: payload.sub,
      uuid: payload.uuid,
      username: payload.username,
      rules: payload.rules,
      profile: payload.profile,
      mfaRequired: payload.mfaRequired,
      mfaPassed: payload.mfaRequired,
    };
  }
}
