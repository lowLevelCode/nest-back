/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import {
  Controller,
  Post,
  UseGuards,
  Body,
  Request,
  Get,
  Param,
} from '@nestjs/common';
import { LocalAuthGuard } from './guards/local/local-auth.guard';
import { AuthService } from './auth.service';
import { LoginDTO } from './dto/login.dto';
import { LoginResponseDTO } from './dto/loginresponse.dto';
import {
  ApiUnauthorizedResponse,
  ApiTags,
  ApiBody,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiBearerAuth,
} from '@nestjs/swagger';
import { JwtAuthGuard } from './guards/jwt/jwt-auth.guard';
import { PingResponseDTO } from './dto/pingresponse.dto';
import { MfaValidationDTO } from './dto/mfavalidation.dto';
import { UsersService } from '../modules/users/users.service';
/**
 * Encargado de la autenticación del usuario.
 */
@ApiTags('auth')
@Controller('auth') // route = /auth
export class AuthController {
  constructor(
    private authService: AuthService,
    private readonly usersService: UsersService,
  ) {}
  /**
   * Ruta de inicialización.
   *
   * *Todos estos comentarios* deben ir antes de los decoradores.
   *
   * @param {LoginDTO} body Cuerpo de la solicitud
   * @param req {Req extraido para obtener o inyectar el usuario}
   */
  @UseGuards(LocalAuthGuard) //solo visible cuando lleva un user (puesto ahi por passport, extraido del body)
  @Post('login') //auth/login
  @ApiBody({
    type: LoginDTO,
    description:
      'La solicitud debe incluir <username> y <password> en el body.',
  })
  @ApiCreatedResponse({
    type: LoginResponseDTO,
    description: 'El usuario ha iniciado sesión.',
  })
  @ApiUnauthorizedResponse({
    description: 'Las credenciales no son válidas.',
  })
  async login(
    @Body() body: LoginDTO,
    @Request() req,
  ): Promise<LoginResponseDTO> {
    return this.authService.login(req.user);
  }

  /**
   * Esta ruta es llamada por el front para verificar la validéz del token
   * que se pretende usar al inicializar al usuario.
   *
   * Hay que considerar si se usa o no.
   */
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Get('ping')
  @ApiOkResponse({
    description: 'El usuario tiene un jwt válido',
  })
  async ping(): Promise<PingResponseDTO> {
    return { result: 'OK' };
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Post('validate-mfa')
  @ApiBody({ type: MfaValidationDTO, description: 'Cuerpo de la solicitud' })
  @ApiCreatedResponse({
    type: LoginResponseDTO,
    description: 'Se ha creado un nuevo token que ya puede pasar la policy',
  })
  async validateMFA(
    @Param('code') code,
    @Request() req,
  ): Promise<LoginResponseDTO | boolean> {
    //validacion del code con speak easy.

    //si es valido, emitir nuevo jwt, con mfaPassed=true

    return this.authService.validateCode(code, req.user);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Get('/setup-mfa')
  async setupMFA() {
    return await this.usersService.setupMFA();
  }
}
