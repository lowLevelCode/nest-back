import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { WSMessageBodyDTO } from './messagebody.dto';
import { verify } from 'jsonwebtoken';
import { LoginIdentityDTO } from '../auth/dto/loginIdentity.dto';
import { JWTPayLoadDTO } from '../auth/dto/jwtPayload.dto';
import { ConfigService } from '@nestjs/config';
import { ConfigKeys } from '../common/enum/configkeys.enum';
/**
 * Guard para socket, utilizamos la misma metodología...
 */
@Injectable()
export class WebsocketGuard implements CanActivate {
  /**
   * Servicios requeridos
   * @param _configService
   */
  constructor(private readonly _configService: ConfigService) {}
  /**
   * Puede o no puede?
   * @param context
   */
  async canActivate(context: ExecutionContext): Promise<boolean> {
    //obtenemos el cliente del contexto para insertarle su identidad
    const wsClient = context.switchToWs().getClient();

    // data del contexto, será modificada
    const data: WSMessageBodyDTO = context.switchToWs().getData();

    //validamos el token
    const decoded: JWTPayLoadDTO = this.validateJWT(data.authorization);

    if (decoded && decoded.sub) {
      const identity: LoginIdentityDTO = {
        uuid: decoded.uuid,
        id: decoded.sub,
        username: decoded.username,
        rules: decoded.rules,
        profile: decoded.profile,
        mfaRequired: decoded.mfaRequired,
        mfaPassed: decoded.mfaRequired,
      };
      wsClient.user = identity; //inserción de identidad en el cliente
      return true; //ok, pasele
    }

    //el guard no pasa
    return false;
  }
  /**
   * Validación del token con la libreria
   * @param jwt
   */
  validateJWT(jwt: string): any {
    let decoded;
    try {
      decoded = verify(
        jwt,
        this._configService.get<string>(ConfigKeys.JWT_SECRET),
      );
    } catch (error) {
      return false;
    }
    return decoded;
  }
}
