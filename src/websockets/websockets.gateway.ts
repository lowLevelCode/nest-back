import {
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
  MessageBody,
  ConnectedSocket,
} from '@nestjs/websockets';
import {
  OnGatewayInit,
  OnGatewayConnection,
  OnGatewayDisconnect,
} from '@nestjs/websockets';
import { Server } from 'socket.io';
import { UseGuards } from '@nestjs/common';
import { Socket } from 'socket.io-client';
import { MyLogger } from '../common/services/logger.service';
import { WebsocketGuard } from './websocket.guard';
import { ParseWSBodyPipe } from './parsewsbody.pipe';

/**
 * Gateway para websockets
 */
@UseGuards(WebsocketGuard)
@WebSocketGateway()
export class WebsocketsGateway
  implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
  /**
   * UnicoServicio
   * @param _loggerService
   */
  constructor(private readonly _loggerService: MyLogger) {}

  /**
   * Evento channels
   */
  @WebSocketServer()
  server: Server;
  @SubscribeMessage('channels')
  async channels(
    @MessageBody(ParseWSBodyPipe) data: { any },
    @ConnectedSocket() client: Socket,
  ): Promise<string[]> {
    const usrname = client.user.username;
    const profile = client.user.profile;
    this._loggerService.verbose(
      `WebsocketsGateway->Event->channels: ${usrname} (${profile}): ${JSON.stringify(
        data,
      )}`,
    );

    //lo unimos a su canal de aplicacion, perfil y usuario
    client.join('app');
    this._loggerService.verbose(
      `WebsocketsGateway->Event->channels->client(${client.id})->joined channel: 'app'`,
    );
    client.join('profile_' + profile);
    this._loggerService.verbose(
      `WebsocketsGateway->Event->channels->client(${
        client.id
      })->joined channel: '${'profile_' + profile}'`,
    );
    client.join('user_' + usrname);
    this._loggerService.verbose(
      `WebsocketsGateway->Event->channels->client(${
        client.id
      })->joined channel: '${'user_' + usrname}'`,
    );

    return ['profile_' + profile, 'user_' + usrname, 'app'];
  }
  /**
   * Después de la inicialización
   */
  async afterInit(): Promise<any> {
    this._loggerService.verbose('WebsocketsGateway initialized.');
    return;
  }
  /**
   * Cada que un cliente se desconecta...
   * @param { Socket } client
   */
  async handleDisconnect(client: Socket): Promise<any> {
    this._loggerService.log(
      'WebsocketsGateway->handleDisconnect->id: ' + client.id,
    );
  }
  /**
   * Cada que un cliente intenta conectarse...
   * @param { Socket } client
   */
  async handleConnection(client: Socket): Promise<any> {
    this._loggerService.log(
      'WebsocketsGateway->handleConnection->id: ' + client.id,
    );
    return;
  }
}
