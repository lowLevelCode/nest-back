import { Module } from '@nestjs/common';
import { WebsocketsGateway } from './websockets.gateway';
import { MyLogger } from '../common/services/logger.service';
import { UsersModule } from '../modules/users/users.module';
/**
 * Modulo de websockets
 */
@Module({
  providers: [WebsocketsGateway, MyLogger],
  imports: [MyLogger, UsersModule],
  exports: [MyLogger],
})
export class EventsModule {}
