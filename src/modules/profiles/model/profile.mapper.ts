import { ProfileDTO } from './profile.dto';
import { ProfileEntity } from './profile.entity';
/**
 * @ignore
 */
export class ProfileMapper {
  dtoToEntity(ProfileDTO: ProfileDTO): ProfileEntity {
    return new ProfileEntity(ProfileDTO.id, ProfileDTO.name, ProfileDTO.rules);
  }
  entityToDto(ProfileEntity: ProfileEntity): ProfileDTO {
    return new ProfileDTO(
      ProfileEntity.id,
      ProfileEntity.name,
      ProfileEntity.rules,
    );
  }
}
