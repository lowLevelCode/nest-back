import { Injectable } from '@nestjs/common';
import { ProfileEntity } from './model/profile.entity';
import { ProfileDTO } from './model/profile.dto';
import { ProfileMapper } from './model/profile.mapper';
import { ProfilesRepository } from './profiles.repository';
/**
 * Service de profiles
 */
@Injectable()
export class ProfilesService {
  constructor(
    private _profilesRepository: ProfilesRepository,
    private _mapper: ProfileMapper,
  ) {}

  async getAllProfiles(): Promise<ProfileDTO[]> {
    const profiles: ProfileEntity[] = await this._profilesRepository.getAllProfiles();
    return profiles.map((profile) => this._mapper.entityToDto(profile));
  }

  async getProfileById(id: number): Promise<ProfileDTO> {
    const profile: ProfileEntity = await this._profilesRepository.getProfileById(
      id,
    );
    return this._mapper.entityToDto(profile);
  }

  async getProfileByName(name: string): Promise<ProfileEntity> {
    return await this._profilesRepository.getProfileByName(name);
  }

  async newProfile(profileDTO: ProfileDTO): Promise<ProfileDTO> {
    const newProfile: ProfileEntity = await this._profilesRepository.newProfile(
      profileDTO,
    );
    return this._mapper.entityToDto(newProfile);
  }

  async updateProfile(id: number, profileDTO: ProfileDTO): Promise<ProfileDTO> {
    const updateProfile = await this._profilesRepository.updateProfile(
      id,
      profileDTO,
    );
    return this._mapper.entityToDto(updateProfile);
  }

  async deleteProfile(id: number): Promise<void> {
    await this._profilesRepository.deleteProfile(id);
  }
}
