import { ProfileDTO } from './model/profile.dto';
import { RuleDTO } from '../rules/model/rule.dto';
/**
 * Rules para admin
 */
const adminRules: RuleDTO[] = [
  {
    name: 'Ver Usuarios',
    description: 'Ver listado de usuarios',
    value: 'users_view',
  },
  {
    name: 'Crear Usuarios',
    description: 'Crear usuarios',
    value: 'users_create',
  },
  {
    name: 'Modificar Usuarios',
    description: 'Modificación de usuarios',
    value: 'users_update',
  },
  {
    name: 'Eliminar usuarios',
    description: 'Eliminación de usuarios',
    value: 'users_delete',
  },

  {
    name: 'Ver Perfiles',
    description: 'Ver listado de Perfiles',
    value: 'profiles_view',
  },
  {
    name: 'Crear Perfiles',
    description: 'Crear Perfiles',
    value: 'profiles_create',
  },
  {
    name: 'Modificar Perfiles',
    description: 'Modificación de Perfiles',
    value: 'profiles_update',
  },
  {
    name: 'Eliminar Perfiles',
    description: 'Eliminación de Perfiles',
    value: 'profiles_delete',
  },

  {
    name: 'Ver Permisos',
    description: 'Ver listado de Permisos',
    value: 'rules_view',
  },
  {
    name: 'Crear Permisos',
    description: 'Crear Permisos',
    value: 'rules_create',
  },
  {
    name: 'Modificar Permisos',
    description: 'Modificación de Permisos',
    value: 'rules_update',
  },
  {
    name: 'Eliminar Permisos',
    description: 'Eliminación de Permisos',
    value: 'rules_delete',
  },
  {
    name: 'Ver Log de Sistema',
    description: 'Permite ver el log de sistema',
    value: 'syslog_view',
  },
];
/**
 * Rules para user
 */
const userRules: RuleDTO[] = [
  {
    name: 'Ver Escritorio',
    description: 'Ver el escritorio de usuario',
    value: 'dashboard_view',
  },
];
/**
 * Perfiles en sistema
 */
export const profiles: ProfileDTO[] = [
  {
    id: null,
    name: 'super',
  },
  {
    id: null,
    name: 'admin',
    rules: adminRules,
  },
  {
    id: null,
    name: 'user',
    rules: userRules,
  },
];
