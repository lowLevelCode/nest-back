import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { ProfileDTO } from './model/profile.dto';
import { ProfileEntity } from './model/profile.entity';
import { ProfileMapper } from './model/profile.mapper';
/**
 * Repositorio para profiles
 */
@Injectable()
export class ProfilesRepository {
  constructor(
    @InjectRepository(ProfileEntity)
    private profilesRepository: Repository<ProfileEntity>,
    private mapper: ProfileMapper,
  ) {}

  getAllProfiles(): Promise<ProfileEntity[]> {
    return this.profilesRepository.find({
      relations: ['rules'],
    });
  }

  getProfileById(id: number): Promise<ProfileEntity> {
    return this.profilesRepository.findOne(id, {
      relations: ['rules'],
    });
  }

  getProfileByName(name: string): Promise<ProfileEntity> {
    return this.profilesRepository.findOne(
      { name: name },
      { relations: ['rules'] },
    );
  }

  async newProfile(profileDTO: ProfileDTO): Promise<ProfileEntity> {
    const newProfile = this.mapper.dtoToEntity(profileDTO);
    return this.profilesRepository.save(newProfile);
  }

  async updateProfile(
    id: number,
    profileDTO: ProfileDTO,
  ): Promise<ProfileEntity> {
    const updateProfileDTO: ProfileDTO = new ProfileDTO(id, profileDTO.name);
    const updateProfile = this.mapper.dtoToEntity(updateProfileDTO);
    await this.profilesRepository.update(id, updateProfile);
    return this.profilesRepository.findOne(id);
  }

  deleteProfile(id: number): Promise<DeleteResult> {
    return this.profilesRepository.delete(id);
  }
}
