import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';
/**
 * @ignore
 */
@Entity('rules')
export class RuleEntity {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column({
    type: 'varchar',
    length: 50,
    nullable: false,
  })
  name: string;

  @Column({
    type: 'varchar',
    length: 150,
    nullable: true,
  })
  description: string;

  @Column({
    unique: true,
    type: 'varchar',
    length: 50,
    nullable: false,
  })
  value: string;

  constructor(id: number, name: string, description: string, value: string) {
    this.name = name;
    this.value = value;
    this.description = description;
  }
}
