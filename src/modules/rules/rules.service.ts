import { Injectable } from '@nestjs/common';
import { RuleEntity } from './model/rule.entity';
import { RulesRepository } from './rules.repository';
import { RuleDTO } from './model/rule.dto';
import { RuleMapper } from './model/rule.mapper';
/**
 * Rules Service
 */
@Injectable()
export class RulesService {
  constructor(
    private _rulesRepository: RulesRepository,
    private _mapper: RuleMapper,
  ) {}

  async getAllRules(): Promise<RuleDTO[]> {
    const rules: RuleEntity[] = await this._rulesRepository.getAllRules();
    return rules.map((rule) => this._mapper.entityToDto(rule));
  }

  async getRuleById(id: number): Promise<RuleDTO> {
    const rule: RuleEntity = await this._rulesRepository.getRuleById(id);
    return this._mapper.entityToDto(rule);
  }

  async getRuleByValue(name: string): Promise<RuleEntity> {
    return await this._rulesRepository.getRuleByValue(name);
  }

  async newRule(ruleDTO: RuleDTO): Promise<RuleDTO> {
    const newRule: RuleEntity = await this._rulesRepository.newRule(ruleDTO);
    return this._mapper.entityToDto(newRule);
  }

  async updateRule(id: number, ruleDTO: RuleDTO): Promise<RuleDTO> {
    const updateRule = await this._rulesRepository.updateRule(id, ruleDTO);
    return this._mapper.entityToDto(updateRule);
  }

  async deleteRule(id: number): Promise<void> {
    await this._rulesRepository.deleteRule(id);
  }
}
