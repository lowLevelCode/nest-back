/**
 * @ignore
 */
export interface Definition {
  id: number;
  type: string;
  name: string;
  title: string;
  label: string;
  order: number;
  icon: string;
}
