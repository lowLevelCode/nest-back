import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';
/**
 * Entidad para definiciones
 */
@Entity('definitions')
export class DefinitionsEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: 'varchar',
    default: 'catalogo',
    length: 50,
    nullable: false,
  })
  type: string;

  @Column({
    type: 'varchar',
    length: 50,
    nullable: false,
  })
  name: string;

  @Column({
    type: 'varchar',
    length: 50,
    nullable: false,
  })
  title: string;

  @Column({
    type: 'varchar',
    default: 'Valor',
    length: 50,
    nullable: false,
  })
  label: string;

  @Column({
    type: 'number',
    default: 0,
    nullable: false,
  })
  order: number;

  @Column({
    type: 'varchar',
    length: 50,
    nullable: false,
  })
  icon: string;

  constructor(
    id: number,
    type: string,
    name: string,
    title: string,
    label: string,
    order: number,
    icon: string,
  ) {
    this.id = id;
    this.type = type;
    this.name = name;
    this.title = title;
    this.label = label;
    this.order = order;
    this.icon = icon;
  }
}
