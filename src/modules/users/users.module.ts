import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from './model/user.entity';
import { UserMapper } from './model/user.mapper';
import { UsersController } from './users.controller';
import { UsersRepository } from './users.repository';
import { UsersService } from './users.service';
import { MyLogger } from '../../common/services/logger.service';
/**
 * Módulo de usuarios
 */
@Module({
  imports: [TypeOrmModule.forFeature([UserEntity])],
  controllers: [UsersController],
  providers: [UserMapper, UsersRepository, UsersService, MyLogger],
  exports: [UserMapper, UsersRepository, UsersService],
})
export class UsersModule {}
