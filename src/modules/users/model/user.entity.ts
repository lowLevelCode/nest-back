import {
  Generated,
  JoinColumn,
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToOne,
  BeforeInsert,
  AfterInsert,
} from 'typeorm';
import { ProfileEntity } from '../../profiles/model/profile.entity';
import { MyLogger } from '../../../common/services/logger.service';
/**
 * @ignore
 */
@Entity('users')
export class UserEntity {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column({ unique: true })
  @Generated('uuid')
  uuid?: string;

  @Column({
    unique: true,
    name: 'username',
    type: 'varchar',
    length: 50,
    nullable: false,
  })
  username: string;

  @Column({
    unique: true,
    type: 'varchar',
    name: 'email',
    length: 150,
    nullable: false,
  })
  email: string;

  @Column({
    type: 'varchar',
    length: 50,
    nullable: false,
  })
  firstName: string;

  @Column({
    type: 'varchar',
    length: 50,
    nullable: false,
  })
  lastName: string;

  @Column({
    type: 'varchar',
    length: 150,
    nullable: false,
  })
  password: string;

  @Column({
    type: 'boolean',
    default: true,
  })
  active?: boolean;

  @Column({
    type: 'simple-array',
    nullable: true,
  })
  rules?: string[];

  @Column({
    type: 'varchar',
    length: 500,
    nullable: true,
  })
  mfaSecret?: string;
  @Column({
    type: 'varchar',
    length: 500,
    nullable: true,
  })
  mfaCreated?: string;
  @Column({
    type: 'varchar',
    length: 500,
    nullable: true,
  })
  mfaOTP?: string;

  @Column({
    type: 'simple-array',
    nullable: true,
  })
  mfaSafeCodes?: string[];

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  @OneToOne((type) => ProfileEntity)
  @JoinColumn()
  profile: ProfileEntity; //la columna profile, es un Profiles

  @BeforeInsert() //lo mismo para Insert/Update/Remove
  createActive(): void {
    const myLogger = new MyLogger();
    this.active = true;
    myLogger.log('Antes de insert');
  }

  @AfterInsert()
  created(): void {
    const myLogger = new MyLogger();
    myLogger.log('Despues de insert');
  }

  constructor(
    id: number,
    uuid: string,
    username: string,
    email: string,
    firstName: string,
    lastName: string,
    password: string,
    active: boolean,
    rules: string[],
    profile: ProfileEntity,
  ) {
    this.id = id;
    this.uuid = uuid;
    this.username = username;
    this.email = email;
    this.firstName = firstName;
    this.lastName = lastName;
    this.password = password;
    this.active = active;
    this.rules = rules;
    this.profile = profile;
  }
}
