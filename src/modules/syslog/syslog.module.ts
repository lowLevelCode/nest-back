import { Module } from '@nestjs/common';
import { SyslogController } from './syslog.controller';
import { SyslogService } from './syslog.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SyslogEntity } from './model/syslog.entity';
import { SyslogRepository } from './syslog.repository';
/**
 * Por el momento el módulo solo trata con entidades, no hay dto's
 */
@Module({
  imports: [TypeOrmModule.forFeature([SyslogEntity])],
  controllers: [SyslogController],
  providers: [SyslogRepository, SyslogService],
  exports: [SyslogRepository, SyslogService],
})
export class SyslogModule {}
