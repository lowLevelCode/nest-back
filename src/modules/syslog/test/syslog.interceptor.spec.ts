import { SyslogInterceptor } from '../syslog.interceptor';

describe('SyslogInterceptor', () => {
  it('should be defined', () => {
    expect(new SyslogInterceptor()).toBeDefined();
  });
});
