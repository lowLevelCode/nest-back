import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { SyslogEntity } from './model/syslog.entity';
/**
 * Syslog Repository
 */
@Injectable()
export class SyslogRepository {
  constructor(
    @InjectRepository(SyslogEntity)
    private _syslogRepository: Repository<SyslogEntity>,
  ) {}

  getAll(): Promise<SyslogEntity[]> {
    return this._syslogRepository.find({
      relations: ['user', 'user.profile'],
    });
  }

  getById(id: number): Promise<SyslogEntity> {
    return this._syslogRepository.findOne(id, {
      relations: ['user', 'user.profile'],
    });
  }

  async newSyslog(log: SyslogEntity): Promise<SyslogEntity> {
    return this._syslogRepository.save(log);
  }
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  async find(query: any): Promise<SyslogEntity[]> {
    return this._syslogRepository.find(query);
  }
}
