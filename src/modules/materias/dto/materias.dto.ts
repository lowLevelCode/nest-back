import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsString } from 'class-validator';
/**
 * @ignore
 */
export class MateriasDTO {
  @ApiProperty()
  @IsNumber()
  id: number;
  @ApiProperty()
  @IsString()
  nombre: string;
}
