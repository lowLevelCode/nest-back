import { Injectable } from '@nestjs/common';
import { MateriasDTO } from './dto/materias.dto';
import { NewMateriasDTO } from './dto/newMaterias.dto';
/**
 * Servicio de materias
 */
@Injectable()
export class MateriasService {
  materias: MateriasDTO[] = [];
  async getAll(): Promise<MateriasDTO[]> {
    return this.materias;
  }

  async create(materia: NewMateriasDTO): Promise<MateriasDTO> {
    const newMateria: MateriasDTO = {
      id: parseInt((Math.random() * 100000000).toFixed(0)),
      nombre: materia.nombre,
    };
    this.materias.push(newMateria);
    return newMateria;
  }
}
