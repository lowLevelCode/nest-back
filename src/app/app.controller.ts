/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import {
  Controller,
  Get,
  Header,
  HttpStatus,
  HttpCode,
  Response,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { MyLogger } from '../common/services/logger.service';
import { InjectPdf, PDF } from 'nestjs-pdf';
import { createReadStream } from 'fs';
import { AppService } from './app.service';
/**
 * Controller principal de la aplicacion
 */
@ApiTags('main')
@Controller() // route = /
export class AppController {
  constructor(
    private readonly _logService: MyLogger,
    private readonly _appService: AppService,
    @InjectPdf()
    private readonly _pdfService: PDF,
  ) {}

  /**
   * Email test
   */
  @Get('email')
  testEmail(): boolean {
    return this._appService.testEmail();
  }

  /**
   * PDF Generation!
   *
   * Intento de generación con plantillas ejs y variables.
   */
  @Get('pdf-example')
  @HttpCode(HttpStatus.CREATED)
  @Header('Content-Type', 'application/pdf')
  @Header('Content-Disposition', 'attachment; filename=pdf-example.pdf')
  async generatePdf(@Response() response): Promise<any> {
    const result = await this._pdfService({
      filename: './upload/pdf-example.pdf', // where pdf will be generated. Generally comprises of the path and filename
      template: 'template-example',
      locals: {
        foo: 'works',
      },
    });
    this._logService.verbose('Generated pdf file: ' + result.filename);
    //open the pdf file and return it in the request

    return createReadStream(result.filename).pipe(response);
  }

  /**
   * Hello world!
   */
  @Get()
  async getHome(): Promise<string> {
    return this._appService.getHome();
  }
}
