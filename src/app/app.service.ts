import { Injectable } from '@nestjs/common';
import { ProfilesService } from '../modules/profiles/profiles.service';
import { UsersService } from '../modules/users/users.service';
import { MyLogger } from '../common/services/logger.service';
import { RulesService } from '../modules/rules/rules.service';

import { UserEntity } from '../modules/users/model/user.entity';
import { ProfileEntity } from '../modules/profiles/model/profile.entity';

import { users as requiredUsers } from '../modules/users/users.collection';
import { profiles as requiredProfiles } from '../modules/profiles/profiles.collection';
import { ProfileDTO } from '../modules/profiles/model/profile.dto';
import { RuleDTO } from '../modules/rules/model/rule.dto';
import { UserDTO } from '../modules/users/model/user.dto';
import { genSalt, hash } from 'bcryptjs';
import { ConfigService } from '@nestjs/config';
import { ConfigKeys } from '../common/enum/configkeys.enum';
import { ISendMailOptions } from '@nestjs-modules/mailer';
import { EmailSenderService } from '../common/services/email-sender.service';
/**
 * Sevicio de app
 */
@Injectable()
export class AppService {
  /**
   * Constructor!
   *
   * @param {ProfilesService} _profiles
   * @param _users
   * @param _logger
   * @param _rules
   */
  constructor(
    private readonly _profiles: ProfilesService,
    private readonly _users: UsersService,
    private readonly _logger: MyLogger,
    private readonly _rules: RulesService,
    private readonly _emailSenderService: EmailSenderService,
    private readonly _configService: ConfigService,
  ) {}
  /**
   * Este método no hace nada
   */
  getHome(): string {
    return 'Hello World!';
  }

  /**
   * Este método intenta inicializar los registros necesarios en las tablas de
   * users, profiles, rules
   */
  async initDatabase(): Promise<boolean> {
    //verificacion de profiles
    this._logger.verbose('Verifying profiles...');

    for (let pIdx = 0; pIdx < requiredProfiles.length; pIdx++) {
      const profile = requiredProfiles[pIdx];
      //verificar que exista el primer usuario admin
      const prof = await this._profiles.getProfileByName(profile.name);

      if (prof && prof.id) {
        this._logger.verbose(
          `Profile "${profile.name}" already exists in current database.`,
        );
      } else {
        //crear rules!
        const createdRules: RuleDTO[] = [];
        if (
          requiredProfiles[pIdx].rules &&
          requiredProfiles[pIdx].rules.length
        ) {
          this._logger.verbose(
            'Preparando reglas para el perfil ' + profile.name,
          );
          //crear las rules para este perfil...
          for (
            let rIdx = 0;
            rIdx < requiredProfiles[pIdx].rules.length;
            rIdx++
          ) {
            const rule: RuleDTO = requiredProfiles[pIdx].rules[rIdx];
            //verificar que exista, aqui nos importa el value, no el name
            const rul = await this._rules.getRuleByValue(rule.value);
            if (rul && rul.id) {
              createdRules.push(rul);
              this._logger.verbose(
                `Rule "${rul.name}" already exists in current database.`,
              );
            } else {
              const newRule = await this._rules.newRule(rule);
              createdRules.push(newRule);
              this._logger.log(
                `Rule "${newRule.name}" (for profile "${profile.name}") has been created successfully.`,
              );
            }
          }
        }

        const profileToCreate: ProfileDTO = {
          name: profile.name,
          rules: createdRules,
        };
        //crear el profile con sus reglas!
        const newProfile: ProfileEntity = await this._profiles.newProfile(
          profileToCreate,
        );

        if (
          !requiredProfiles[pIdx].rules ||
          !requiredProfiles[pIdx].rules.length
        ) {
          this._logger.warn(`Profile "${newProfile.name}" has no rules!.`);
        }

        this._logger.log(
          `Profile "${newProfile.name}" has been created successfully.`,
        );
      }
    }

    if (!requiredProfiles.length) {
      this._logger.warn(`No profiles has been found.`);
    }

    this._logger.verbose('Profiles verification succeded.');

    if (this._configService.get<string>(ConfigKeys.CREATE_USERS)) {
      this._logger.verbose('Verifying users... ');

      for (let pIdx = 0; pIdx < requiredUsers.length; pIdx++) {
        const user: UserDTO = requiredUsers[pIdx];
        const userFound: UserEntity = await this._users.getUserByUsername(
          user.username,
        );

        if (userFound && userFound.id) {
          this._logger.verbose(
            `User "${userFound.username}" already exists in current database.`,
          );
        } else {
          this._logger.verbose(`Creating user "${user.username}"...`);

          const salt = await genSalt(10);
          user.password = await hash(
            this._configService.get<string>(ConfigKeys.FIRST_PASSWORD),
            salt,
          );

          const newUser: UserDTO = await this._users.newUser(user);

          this._logger.log(
            `User "${
              newUser.username
            }" with password "${this._configService.get<string>(
              ConfigKeys.FIRST_PASSWORD,
            )}" has been created successfully.`,
          );
        }
      }

      this._logger.verbose('Users verification succeded.');
    } else {
      this._logger.warn(
        `Users verification not performed, environment var CREATE_USERS: {.env.CREATE_USERS}.`,
      );
    }

    return true;
  }

  /**
   * Ejemplo usando un service inyectable: EmailSenderService
   *
   * El service se hace cargo de validar si envia o nó los correos
   * segun la configuración en .env
   */
  testEmail(): boolean {
    const email: ISendMailOptions = {
      to: this._configService.get<string>(ConfigKeys.SMTP_FROM_EMAIL),
      subject: 'Testing Nest MailerModule ✔', // Subject line
      template: 'welcome',
      context: {
        username: 'ecorona',
        foo: 'works',
      },
    };
    return this._emailSenderService.send(email);
  }
}
